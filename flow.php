<?php $id="flow";?>
<?php include($_SERVER['DOCUMENT_ROOT'] . '/assets/include/header.php'); ?>
	<!--***** main section *****-->
	<div class="p-flow">
		<div class="l-main">
			<!-- section 1 -->
			<section class="p-flow1">
				<!-- breadcrumb -->
				<div class="l-breadcrumb">
					<ul>
						<li><a href="">トップページ</a></li>
						<li>住まいづくりのすすめ方</li>
					</ul>
				</div>
				<div class="p-flow1__title">
					<h3><img src="assets/image/flow/title.png" alt="title" width="302" height="40"></h3>
				</div>
				<div class="p-flow1__image">
					<img src="assets/image/flow/img1.png" alt="" width="708" height="295">
				</div>
				<div class="p-flow1__text">
					<p>住宅にせよ、業務用の建物にせよ、初めての方が大半だと思います。<br>
					「何から始めて良いか？」わからないのは当然です。<br>
					そういった時に、最初に相談する窓口が我々「建築設計事務所」です。<br>
					一級建築士事務所SHEaPでは、建物のことだけでなく、<br>
					住まいづくりにおける資金計画や土地のこと、<br>
					事業用の土地や運営計画まで、<br>
					建築に関わる初期段階の相談を承っております。<br>
					気軽にお声かけください。</p>

					<p>下記に、住宅の場合を例に大まかなスケジュールを表示しましたので、ごらんください。</p>
				</div>
				<ul class="p-flow1__image">
					<li><img src="assets/image/flow/s1.png" alt="" width="707" height="293"></li>
					<li><img src="assets/image/flow/s2.png" alt="" width="708" height="303"></li>
					<li><img src="assets/image/flow/s3.png" alt="" width="697" height="297"></li>
					<li><img src="assets/image/flow/s4.png" alt="" width="708" height="305"></li>
					<li><img src="assets/image/flow/s5.png" alt="" width="707" height="295"></li>
				</ul>
			</section>
			<!-- section 2 -->
			<section class="p-flow2">
				<div class="p-flow2__block">
					<div class="p-flow2__title"><h3>設計費について</h3></div>
					<div class="p-flow2__text">
						<p>設計事務所の住まいづくりは、決して高くはありません。</p>
						<p>よく、「設計事務所に依頼すると、設計費が余分にかかるんじゃないの？」と言われますが、決してそうではありません。ハウスメーカー、工務店、大工さんに依頼する場合も、設計費という項目がない会社もありますが、必ず設計費はかかっているのです。</p>
						<p>設計事務所から工務店に依頼する場合、当然、設計費を抜いた金額で依頼する為、同じ建物を建てるならば、設計施工の会社に依頼する場合と比べて高くなるということはありません。</p>
						<p>もちろん、条件が難しかったり、特別なこだわりがある方の場合は、工事も含めそれなり費用になりますが、そういった場合が設計事務所としての腕の見せ所だと思っていますし、得意としてます。</p>
					</div>
				</div>
				<div class="p-flow2__block">
					<div class="p-flow2__title"><h3>費用の目安</h3></div>
					<div class="p-flow2__text">
						<p>計画建物の大きさや、敷地、申請、用途などの条件によってさまざまですが、<br>
							おおよそ、建築工事費の6％～10％です。<br>
							建物が大きいほど、スケールメリットがでて、設計費も安くなります。</p>
						<p>30～40坪で、基本計画、実施設計、設計監理、外構計画、インテリアコーディネートなど、全て含んで、工事金額の8％くらいが目安となります。</p>
					</div>
				</div>
			</section>
			<!-- section 3 -->
			<section class="p-flow3">
				<img src="assets/image/common/contact-info.png" alt="" width="442" height="148">
				<a href="">
					<img src="assets/image/common/button_contact2.png" alt="" width="140" height="140">
				</a>
			</section>
		</div>
	</div>
</div>
<?php include($_SERVER['DOCUMENT_ROOT'] . '/assets/include/footer.php'); ?>