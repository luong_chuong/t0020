<!DOCTYPE html>
<html lang="ja" id="pagetop">
<head>
<meta charset="UTF-8">
<meta name="format-detection" content="telephone=no">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=no">
<?php include($_SERVER['DOCUMENT_ROOT'] . '/assets/include/meta.php'); ?>
<link href="/assets/js/slick/slick.css" rel="stylesheet">
<link href="/assets/css/style.css" rel="stylesheet">
<script src="/assets/js/jquery-2.2.4.min.js"></script>
</head>
<body class="page-<?php echo $id; ?>">
<!--***** container *****-->
<div class="l-container">
	<a href="" class="c-contact">
		<img src="assets/image/common/button_contact.png" alt="" width="107" height="107">
	</a>
	<!--***** layout side *****-->
	<header class="c-header">
		<div class="l-side">
			<nav class="c-gnavi">
				<a href=""><img src="assets/image/common/logo-top.png" alt="" width="150" height="107"></a>
				<a href="" class="item-home"><img src="assets/image/common/menu_home.png" alt="" width="58" height="65"></a>
				<ul class="u-vertical">
					<li><a href="">Q&A</a></li>
					<li><a href="">ブログ</a></li>
					<li><a href="">住まいづくりのすすめ方</a></li>
					<li><a href="">ギャラリー</a></li>
					<li><a href="">デザインコンセプト</a></li>
					<li><a href="">シープについて</a></li>
				</ul>
				<ul class="u-normal">
					<li>
						<a href="">サイトマップ</a>
					</li>
					<li>
						<a href="">プライバシーポリシー</a>
					</li>
				</ul>
			</nav>
		</div>
	</header>