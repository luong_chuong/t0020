<footer class="c-footer">
	<div class="c-footer__sitemap">
		<div class="c-footer__inner">
			<ul class="top">
				<li><a href="">ホーム</a></li>
				<li class="c-backtop"><a>ページトップへ</a></li>
			</ul>
			<ul class="bottom">
				<li>
					<ul class="block">
						<li><a href="">About <span>シープについて</span></a></li>
						<li><a href="">設計事務所と創りましょう</a></li>
						<li><a href="">事務所概要</a></li>
						<li><a href="">プロフィール</a></li>
						<li><a href="">アクセスマップ</a></li>
					</ul>
					<ul class="block">
						<li><a href="">Gallery <span>ギャラリー</span></a></li>
					</ul>
					<ul class="block">
						<li><a href="">Flow <span>住まいづくりのすすめ方</span></a></li>
					</ul>
				</li>
				<li>
					<ul class="block">
						<li><a href="">Desing <span>デザインコンセプト</span></a></li>
						<li><a href="">光</a></li>
						<li><a href="">風</a></li>
						<li><a href="">省エネ</a></li>
						<li><a href="">安心・安全</a></li>
						<li><a href="">広がり・つながり</a></li>
						<li><a href="">シンプル</a></li>
					</ul>
					<ul class="block">
						<li><a href="">Contact <span>お問い合わせ</span></a></li>
					</ul>
				</li>
				<li>
					<ul class="block">
						<li><a href="">Q&A <span>質問と解答</span></a></li>
					</ul>
					<ul class="block">
						<li><a href="">Blog <span>ブログ</span></a></li>
					</ul>
					<ul class="block-1">
						<li><a href="">プライバシーポリシー</a></li>
						<li><a href="">サイトマップ</a></li>
					</ul>
				</li>
				<li class="social">
					<ul>
						<li>
							<a href=""><span><img src="assets/image/common/icon-fb.png" alt="" width="29" height="29"></span>公式Facebookはこちら</a>
						</li>
						<li><a href=""><span><img src="assets/image/common/icon-social.png" alt="" width="28" height="28"></span>公ひつじ的住まいズム！</a></li>
						<li><a href=""><span><img src="assets/image/common/icon-social.png" alt="" width="28" height="28"></span>ヒツジ的フォトライフ！</a></li>
					</ul>
				</li>
			</ul>
		</div>
	</div>
	<div class="c-footer__info">
		<div class="c-footer__inner">
			<div class="l-flex">
				<div class="c-footer__info--logo">
					<a href=""><img src="assets/image/common/logo-ft.png" alt="" width="147" height="61"></a>
				</div>
				<div class="c-footer__info--contact">
					<p>一級建築士事務所登録　静岡県知事登録（◯-00）第00000号<br>
					〒400-0000静岡県浜松市中区三組町28-76<br>
					TEL：053-000-0000　FAX：053-000-0000</p>
					<button>
						<img src="assets/image/common/button_fb.png" alt="" width="73" height="23">
					</button>
				</div>
			</div>
		</div>
	</div>
	<div class="c-footer__copyright">
		<div class="c-footer__inner">
			<div class="l-flex"><p>Copyright © 2015 SHEaP inc. All Rights Reserved.</p></div>
		</div>
	</div>
</footer>
<script src="/assets/js/slick/slick.js"></script>
<script src="/assets/js/functions.js"></script>
</body>
</html>