/*===================================================
Viewport width fit for Tablet
===================================================*/
var _ua = (function(u){
  return {
    Tablet:(u.indexOf("windows") != -1 && u.indexOf("touch") != -1 && u.indexOf("tablet pc") == -1)
      || u.indexOf("ipad") != -1
      || (u.indexOf("android") != -1 && u.indexOf("mobile") == -1)
      || (u.indexOf("firefox") != -1 && u.indexOf("tablet") != -1)
      || u.indexOf("kindle") != -1
      || u.indexOf("silk") != -1
      || u.indexOf("playbook") != -1,
    Mobile:(u.indexOf("windows") != -1 && u.indexOf("phone") != -1)
      || u.indexOf("iphone") != -1
      || u.indexOf("ipod") != -1
      || (u.indexOf("android") != -1 && u.indexOf("mobile") != -1)
      || (u.indexOf("firefox") != -1 && u.indexOf("mobile") != -1)
      || u.indexOf("blackberry") != -1
  }
})(window.navigator.userAgent.toLowerCase());

if(_ua.Tablet){
  $("meta[name='viewport']").attr('content', 'width=1100');
}

$(document).ready(function(){
  $('.p-top1__slide').slick({
    dots: true,
    infinite: true,
    speed: 700,
    //fade: true,
    arrows: false,
    autoplay: true,
    cssEase: 'linear'
  });

  $(window).scroll(function() {
    var ftTop = $('.c-footer').offset().top;
    console.log($('.c-footer').offset().top - 1000);
    var x = ftTop - 490;
    var y = ftTop - 1500;
    if ($(this).scrollTop() >= x){
      $('.c-gnavi').addClass('is-hide');
    }
    else {
      $('.c-gnavi').removeClass('is-hide');
    }
    if ($(this).scrollTop() >= y){
      $('.c-contact').addClass('is-hide');
    }
    else {
      $('.c-contact').removeClass('is-hide');
    }
  });

  $('.c-backtop').click(function() {
    $("html, body").animate({ scrollTop: 0 }, "slow");
  });

  $('.c-accordion__button').click(function() {
    $(this).parent('li').toggleClass('active');
  });

});

var acc = document.getElementsByClassName('c-accordion__button');
var i;
for (i = 0; i < acc.length; i++) {
  acc[i].addEventListener("click", function() {
    //this.classList.toggle("active");
    var panel = this.nextElementSibling;
    //panel.classList.toggle("active");
    if (panel.style.maxHeight){
      panel.style.maxHeight = null;
    } else {
      panel.style.maxHeight = panel.scrollHeight + "px";
    }
  });
}