<?php $id="top";?>
<?php include($_SERVER['DOCUMENT_ROOT'] . '/assets/include/header.php'); ?>
	<!--***** main section *****-->
	<div class="p-top">
		<div class="l-main">
			<!-- section 1 -->
			<section class="p-top1">
				<div class="p-top1__title">
					<h1><img src="assets/image/common/h1.png" alt="" width="460" height="24"></h1>
				</div>
				<div class="p-top1__slide">
					<div><img src="assets/image/common/slide.jpg" alt=""></div>
					<div><img src="assets/image/common/slide.jpg" alt=""></div>
					<div><img src="assets/image/common/slide.jpg" alt=""></div>
					<div><img src="assets/image/common/slide.jpg" alt=""></div>
					<div><img src="assets/image/common/slide.jpg" alt=""></div>
				</div>
			</section>
			<!-- section 2 -->
			<section class="p-top2">
				<div class="p-top2__title"><h2><img src="assets/image/top/title2.png" alt="" width="586" height="91"></h2></div>
				<div class="p-top2__text">
					<p>	豊かな住まいとは、ただ広く、豪華な素材を使うことではありません <br>
						本当の意味での「豊かな住まい」とは 住まう人に合っていることなのではないでしょうか<br>
						じっくりと話し、ライフスタイルにフィットしたものを見つけ出していく<br>
						一級建築士事務所 SHEaPは、そんな設計を心がけています<br>
					</p>
				</div>
			</section>
			<!-- section 3 -->
			<section class="p-top3">
				<div class="p-top3__image">
					<img src="assets/image/top/img1.png" alt="" width="348" height="427">
					<a href="" class="p-top3__button">
						<img src="assets/image/top/sheap1.png" alt="" width="117" height="134" >
					</a>
				</div>
				<div class="p-top3__content">
					<div class="p-top3__avatar">
						<img src="assets/image/top/avatar.png" alt="" width="162" height="216">
					</div>
					<div class="p-top3__title">
						<div class="c-title">
							<h3><img src="assets/image/top/text-about.png" alt="" width="369" height="112"></h3>
						</div>
					</div>
					<div class="p-top3__text">
						<p>住まいづくりをする方が一番意識するのは、間取りや材料、デザイン、そして、コストとのバランスです。それを総合的に提案するのが設計者の役割だと考えています。</p>
						<p>せっかくの住まいづくりですから、より住まう自分達にあったものを追求して欲しいと思います。</p>
						<p>住まう方にフィットする住まいを提供する為に、お客様とのコミュニケーションを大切にして設計したいと思っています。</p>
					</div>
				</div>
			</section>
			<!-- section 4 -->
			<section class="p-top4">
				<div class="p-top4__content">
					<div class="p-top4__title">
						<div class="c-title">
							<p><img src="assets/image/top/titledesing.png" alt="" width="318" height="46"></p>
							<h3><img src="assets/image/top/text-desing.png" alt="" width="313" height="114"></h3>
						</div>
					</div>
					<div class="p-top4__text">
						<p>一級建築士事務所SHEaPでは、<br>
						「光」「風」「省エネ」「安心・安全」「広がり・つながり」「シンプル」といった6つの設計コンセプトを大切に考えています。</p>
					</div>
				</div>
				<div class="p-top4__image">
					<img src="assets/image/top/imgdesing.png." alt="" width="348" height="438">
					<a href="" class="p-top4__button">
						<img src="assets/image/top/sheap2.png" alt="" width="117" height="134" >
					</a>
				</div>
			</section>
			<!-- section 5 -->
			<section class="p-top5">
				<div class="p-top5__title">
					<div class="c-title">
						<p><img src="assets/image/top/titlegallery1.png" alt="" width="326" height="50"></p>
						<h3><img src="assets/image/top/text-gallery.png" alt="" width="526" height="26"></h3>
					</div>
				</div>
				<div class="p-top5__image">
					<div class="c-gallery">
						<div class="c-gallery__item">
							<a href="">
								<img src="assets/image/top/gallery1.jpg" alt="">
							</a>
						</div>
						<div class="c-gallery__item">
							<a href="">
								<img src="assets/image/top/gallery2.jpg" alt="">
							</a>
						</div>
						<div class="c-gallery__item">
							<a href="">
								<img src="assets/image/top/gallery3.jpg" alt="">
							</a>
						</div>
						<div class="c-gallery__item">
							<a href="">
								<img src="assets/image/top/gallery4.jpg" alt="">
							</a>
						</div>
						<div class="c-gallery__item">
							<a href="">
								<img src="assets/image/top/gallery5.jpg" alt="">
							</a>
						</div>
						<div class="c-gallery__item">
							<a href="">
								<img src="assets/image/top/gallery6.jpg" alt="">
							</a>
						</div>
						<div class="c-gallery__item">
							<a href="">
								<img src="assets/image/top/gallery7.jpg" alt="">
							</a>
						</div>
						<div class="c-gallery__item">
							<a href="">
								<img src="assets/image/top/gallery8.jpg" alt="">
							</a>
						</div>
						<div class="c-gallery__item">
							<a href="">
								<img src="assets/image/top/gallery9.jpg" alt="">
							</a>
						</div>
						<div class="c-gallery__item">
							<a href="">
								<img src="assets/image/top/gallery10.jpg" alt="">
							</a>
						</div>
						<div class="c-gallery__item">
							<a href="">
								<img src="assets/image/top/gallery11.jpg" alt="">
							</a>
						</div>
						<div>
							<a href="">
								<img src="assets/image/top/sheap1.png" alt="" width="117" height="134">
							</a>
						</div>
					</div>
				</div>
			</section>
			<!-- section 6 -->
			<section class="p-top6">
				<div class="p-top6__title">
					<div class="c-title">
						<p><img src="assets/image/top/title-schedule1.png" alt="" width="393" height="39"></p>
						<h3><img src="assets/image/top/text-schedule.png" alt="" width="549" height="26"></h3>
					</div>
				</div>
				<div class="p-top6__text">
					<p>この限りではありませんので、ご要望に合わせてご相談ください。</p>
				</div>
				<div class="p-top6__image">
					<img src="assets/image/top/s1.png" alt="" width="564" height="276">
					<img src="assets/image/top/s2.png" alt="" width="564" height="275">
					<img src="assets/image/top/s3.png" alt="" width="564" height="274">
					<img src="assets/image/top/s4.png" alt="" width="593" height="273">
				</div>
				<div class="p-top6__button">
					<a href="">
						<img src="assets/image/top/sheap3.png" alt="" width="117" height="134">
					</a>
				</div>
			</section>
			<!-- section 7 -->
			<section class="p-top7">
				<div class="p-top7__box">
					<div class="p-top7__title"><h3><img src="assets/image/top/title-blog1.png" alt="" width="171" height="61"></h3></div>
					<div class="p-top7__content">
						<div class="p-top7__content--title">
							<h4>タイトル</h4>
						</div>
						<div class="p-top7__content--text">
							<p>記事内容がここに表示されます。記事内容がここに表示されます。記事内容がここに表示されます。記事内容がここに表示されます。記事内容がここに表示されます。記事内容がここに表示されます。記事内容がここに表示されます。記事内容がここに表示されます。記事内容がここに表示されます。記事内容がここに表示されます。記事内容がここに表示されます。</p>
							<p>記事内容がここに表示されます。記事内容がここに表示されます。記事内容がここに表示されます。記事内容がここに表示されます。<br>
								記事内容がここに表示されます。記事内容がここ表示されます。
							</p>
						</div>
					</div>
					<div class="p-top7__button">
						<a href="">
							<img src="assets/image/top/sheap1.png" alt="" width="117" height="134">
						</a>
					</div>
				</div>
			</section>
			<!-- section 8 -->
			<section class="p-top8">
				<div class="p-top8__title">
					<h3><img src="assets/image/top/title-qa1.png" alt="" width="278" height="38"></h3>
					<div class="p-top8__button">
						<a href="">
							<img src="assets/image/top/sheap4.png" alt="" width="122" height="141">
						</a>
					</div>
				</div>
				<div class="p-top8__content">
					<a href="">
						<div class="c-card">
							<div class="c-card__image"><img src="assets/image/top/fakeimg.png" alt="" width="221" height="125"></div>
							<div class="c-card__text">
								<h4>Q1.</h4>
								<p>設計事務所って<br>高いんじゃないの？</p>
							</div>
						</div>
					</a>
					<a href="">
						<div class="c-card">
							<div class="c-card__image"><img src="assets/image/top/fakeimg.png" alt="" width="221" height="125"></div>
							<div class="c-card__text">
								<h4>Q2.</h4>
								<p>工事は請け負わないの？</p>
							</div>
						</div>
					</a>
					<a href="">
						<div class="c-card">
							<div class="c-card__image"><img src="assets/image/top/fakeimg.png" alt="" width="221" height="125"></div>
							<div class="c-card__text">
								<h4>Q3.</h4>
								<p>小さな会社に依頼しても<br>安心できますか？</p>
							</div>
						</div>
					</a>
					<a href="">
						<div class="c-card">
							<div class="c-card__image"><img src="assets/image/top/fakeimg.png" alt="" width="221" height="125"></div>
							<div class="c-card__text">
								<h4>Q4.</h4>
								<p>設計事務所って<br>高いんじゃないの？</p>
							</div>
						</div>
					</a>
					<a href="">
						<div class="c-card">
							<div class="c-card__image"><img src="assets/image/top/fakeimg.png" alt="" width="221" height="125"></div>
							<div class="c-card__text">
								<h4>Q5.</h4>
								<p>工事は請け負わないの？</p>
							</div>
						</div>
					</a>
					<a href="">
						<div class="c-card">
							<div class="c-card__image"><img src="assets/image/top/fakeimg.png" alt="" width="221" height="125"></div>
							<div class="c-card__text">
								<h4>Q6.</h4>
								<p>小さな会社に依頼しても<br>安心できますか？</p>
							</div>
						</div>
					</a>
				</div>
			</section>
			<!-- section 9 -->
			<section class="p-top9">
				<div class="p-top9__title">
					<h3><img src="assets/image/top/title-contact1.png" alt="" width="274" height="36"></h3>
				</div>
				<div class="p-top9__text">
					<p><img src="assets/image/top/text-contact.png" alt="" width="729" height="70"></p>
				</div>
				<div class="p-top9__form">
					<form action="" id="contact">
						<ul>
							<li>
								<label for="name">お名前<span class="u-required">【必須】</span></label>
								<input type="text" name="name">
							</li>
							<li>
								<label for="email">Email<span class="u-required">【必須】</span></label>
								<input type="email" name="email">
							</li>
							<li>
								<label>お問い合わせ種別</label>
								<div class="checkgroup">
									<label><input type="checkbox" name="type1">新築</label>
									<label><input type="checkbox" name="type2">リフォーム</label>
									<label><input type="checkbox" name="type3">アパート・マンションオーナー様</label>
									<label><input type="checkbox" name="type4">相談会申込み</label>
									<label><input type="checkbox" name="type5">その他</label>
								</div>
							</li>
							<li>
								<label for="postalcode">郵便番号</label>
								<input type="number" name="postalcode">
							</li>
							<li>
								<label for="address">ご住所</label>
								<input type="text" name="address">
							</li>
							<li>
								<label for="phone">お電話番号</label>
								<input type="number" name="phone">
							</li>
							<li>
								<label for="content">お問い合わせ内容<span class="u-required">【必須】</span></label>
								<textarea name="content" id=""></textarea>
							</li>
							<li class="l-btn l-btn--2center">
								<div class="c-btn"><a href="">リセット</a></div>
								<div class="c-btn"><a href="">確認画面へ</a></div>
							</li>
						</ul>
					</form>
				</div>
			</section>
			<!-- section 10 -->
			<section class="p-top10">
				<img src="assets/image/common/contact-info.png" alt="" width="442" height="148">
				<a href="">
					<img src="assets/image/common/button_contact2.png" alt="" width="140" height="140">
				</a>
			</section>
		</div>
	</div>
</div>
<?php include($_SERVER['DOCUMENT_ROOT'] . '/assets/include/footer.php'); ?>